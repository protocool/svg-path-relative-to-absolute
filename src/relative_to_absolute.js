// TODO:apply same principe for each specific commands

//                            Relative to absolute

function extract_commands(path){

  let d = path.getAttribute('d');

  var cmdRegEx = /\n|\t|\r/g;
  d = d.replace(cmdRegEx,'');
  cmdRegEx = /[a-z][^a-z]*/ig;
  var finded_commands = d.match(cmdRegEx);
  let commands = [];

  for ( var i = 0 ; i < finded_commands.length ; i ++ ) {

    var command_name = finded_commands[i].charAt(0);
    let relative = command_name === command_name.toUpperCase() ? false : true;
    var command = [ command_name, [] ];
    
    ptsFinder = /-.+?(?=(-|,| |$))|(?<=[a-z]|[A-Z]|-|,| |$).+?(?=(-|,| |$))/ig;
    let pts = finded_commands[i].match(ptsFinder);
    if ( pts === null ) continue;

    var p = 0;
    while ( p < pts.length ){
      if ( isNaN(parseFloat(pts[p])) ){
        let test = pts.splice(p, 1);
      } else {
        p++;
      };
    };
    
    switch(command_name.toUpperCase()){
      case 'M' : 
      case 'L' : {
        for ( var p = 0 ; p < pts.length ; p += 2 ) {
          command[1].push([parseFloat(pts[p]),parseFloat(pts[p+1])]);
          if ( command[1].length == 1 && p < pts.length-2){
            commands.push(command);
            command_name = relative ? 'l' : 'L' ; 
            command = [ command_name, [] ];
          };
        };
        commands.push(command);
        break; 
      }; 
      case 'V' : 
      case 'H' : {
        for ( var p = 0 ; p < pts.length ; p ++ ) {
          command[1].push([parseFloat(pts[p])]);
          if ( command[1].length == 1 && p < pts.length-1){
            commands.push(command);
            command_name = relative ? command_name.toLowerCase() : command_name.toUpperCase() ; 
            command = [ command_name, [] ];
          };
        };
        commands.push(command);
        break; 
      }; 
      case 'C' : {
        for ( var p = 0 ; p < pts.length ; p += 2 ) {
          command[1].push([parseFloat(pts[p]),parseFloat(pts[p+1])]);
          if ( command[1].length == 3 && p < pts.length-2){
            console.log(command[1]);
            commands.push(command);
            command_name = relative ? 'c' : 'C' ; 
            command = [ command_name, [] ];
          };
        };
        commands.push(command);
        break; 
      }; 
      case 'S' : console.log('S'); break;
      case 'Q' : console.log('Q'); break;
      case 'T' : console.log('T'); break;
      case 'A' : console.log('A'); break;
    };

    continue;
    if (pts,finded_commands[i] == 'z' || finded_commands[i] == 'Z') continue
  };
  return commands;
};

function normalize_commands(commands){
  let last_point = [0,0];
  for ( var c = 0 ; c < commands.length ; c++ ){
    if ( commands[c][0] === commands[c][0].toUpperCase() ) {
      last_point[0] = commands[c][1][commands[c][1].length-1][0];
      last_point[1] = commands[c][1][commands[c][1].length-1][1];
    } else {
      switch(commands[c][0]){
        case 'm' : 
        case 'l' : {
          commands[c][0] = commands[c][0].toUpperCase();
          last_point[0] += commands[c][1][0][0];
          last_point[1] += commands[c][1][0][1];
          commands[c][1][0][0] = last_point[0];
          commands[c][1][0][1] = last_point[1];
          break; 
        }; 
        case 'h' : 
        case 'v' : {
          commands[c][0] = commands[c][0].toUpperCase();
          last_point[0] += commands[c][1][0][0];
          commands[c][1][0][0] = last_point[0];
          commands[c][0] = commands[c][0].toUpperCase();
          break; 
        }; 
        case 'c' : {
          commands[c][0] = commands[c][0].toUpperCase();
          commands[c][1][0][0] += last_point[0];
          commands[c][1][0][1] += last_point[1];
          commands[c][1][1][0] += last_point[0];
          commands[c][1][1][1] += last_point[1];
          last_point[0] += commands[c][1][2][0];
          last_point[1] += commands[c][1][2][1];
          commands[c][1][2][0] = last_point[0];
          commands[c][1][2][1] = last_point[1];
          break; 
        }; 
        case 's' : {
          commands[c][0] = commands[c][0].toUpperCase();
          for ( var p = 0 ; p < commands[c][1].length ; p++ ){
            console.log(p);
            last_point[0] += commands[c][1][p][0];
            last_point[1] += commands[c][1][p][1];
            commands[c][1][p][0] = last_point[0];
            commands[c][1][p][1] = last_point[1];
          };
          break; 
        }; 
        case 'q' : {
          commands[c][0] = commands[c][0].toUpperCase();
          for ( var p = 0 ; p < commands[c][1].length ; p++ ){
            console.log(p);
            last_point[0] += commands[c][1][p][0];
            last_point[1] += commands[c][1][p][1];
            commands[c][1][p][0] = last_point[0];
            commands[c][1][p][1] = last_point[1];
          };
          break; 
        }; 
        case 't' : {
          commands[c][0] = commands[c][0].toUpperCase();
          for ( var p = 0 ; p < commands[c][1].length ; p++ ){
            console.log(p);
            last_point[0] += commands[c][1][p][0];
            last_point[1] += commands[c][1][p][1];
            commands[c][1][p][0] = last_point[0];
            commands[c][1][p][1] = last_point[1];
          };
          break; 
        }; 
        case 'a' : {
          commands[c][0] = commands[c][0].toUpperCase();
          for ( var p = 0 ; p < commands[c][1].length ; p++ ){
            console.log(p);
            last_point[0] += commands[c][1][p][0];
            last_point[1] += commands[c][1][p][1];
            commands[c][1][p][0] = last_point[0];
            commands[c][1][p][1] = last_point[1];
          };
          break; 
        }; 
      };
    };
  };
  return commands;
};

function commands_to_string (commands /* [ [cmd,pts_arr] ... ] */, translation ){
  let new_commands = "";
  for ( var c = 0 ; c < commands.length ; c++ ) {
    new_commands += commands[c][0]+' ';
    switch(commands[c][0]){
      case 'M' : 
      case 'L' :
      case 'C' : {
        for ( var p = 0 ; p < commands[c][1].length ; p++ ) {
          new_commands += (commands[c][1][p][0]+translation[0]) + ", "
                         +  (commands[c][1][p][1]+translation[1]) + ' ' ;
        };
        break; 
      }; 
      case 'H' : { 
        new_commands += (parseFloat(commands[c][1][0])+parseFloat(this.translation[0])) + ' ';
        break; 
      }; 
      case 'V' : {
        new_commands += (parseFloat(commands[c][1][0])+parseFloat(this.translation[0])) + ' ';
        break; 
      }; 
      case 'S' : {
        break; 
      }; 
      case 'Q' : {
        break; 
      }; 
      case 'T' : {
        break; 
      }; 
      case 'A' : {
        break; 
      }; 
    }; 
  };
  new_commands += 'Z';
  return new_commands;
};
