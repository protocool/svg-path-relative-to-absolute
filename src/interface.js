//                                 Interface

const paste_input = document.getElementById("paste");

paste_input.addEventListener("input",function(){
    let parsed_input = new DOMParser().parseFromString( this.value, "text/xml");
    if ( parsed_input.firstChild.tagName == "path" ) {
      let path = document.createElementNS(xmlns,"path");
      path.setAttribute('d',parsed_input.firstChild.getAttribute('d'));
      original.dom.appendChild(path);
      new Form( normalize_commands(extract_commands(path)), converted.dom );
    };
});

const original = new SvgCanvas("original_svg_container",11,11);
const converted = new SvgCanvas("converted_svg_container",11,11);

