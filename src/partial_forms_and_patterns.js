const xmlns = "http://www.w3.org/2000/svg";

const SvgCanvas = function(container,w,h) {
  let boxWidth = w + "cm";
  let boxHeight = h + "cm";
  this.dom = document.createElementNS(xmlns, "svg");
  this.dom.setAttributeNS(null, "width", boxWidth);
  this.dom.setAttributeNS(null, "height", boxHeight);
  this.dom.style.display = "block";
  let defs = document.createElementNS(xmlns, "defs");
  this.dom.appendChild(defs);
  let svgContainer = document.getElementById(container);
  svgContainer.appendChild(this.dom);
  this.dom.setAttributeNS(null, "viewBox", "0 0 " + this.dom.getBoundingClientRect().width + " " + this.dom.getBoundingClientRect().height);
}


const Form = function(commands /* [ [cmd,pts_arr] ... ] */, destination ){
  this.base_commands = commands;
  this.translation = [0,0];
  this.rotation = 0; 
  this.path = document.createElementNS(xmlns, "path");  
  this.path.setAttributeNS(null, 'd', commands_to_string(this.base_commands,this.translation));
  //this.path.setAttributeNS(null, 'fill', ["yellow","cyan","magenta","red","blue","green","black","grey"][parseInt(Math.random()*8)]);
  this.path.setAttributeNS(null, 'opacity', 1.0);
  destination.appendChild(this.path);
  this.center = [this.path.getBBox().width/2,this.path.getBBox().height/2];
  this.origin = [this.path.getBBox().x,this.path.getBBox().y];
};

